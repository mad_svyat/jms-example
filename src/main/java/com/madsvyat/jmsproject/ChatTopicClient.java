package com.madsvyat.jmsproject;

import java.util.Scanner;

import javax.jms.JMSException;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;
import javax.jms.TopicSubscriber;
import javax.naming.InitialContext;

/**
 *
 */
public class ChatTopicClient {

    private static final String TOPIC_NAME = "ChatTopic";

    private TopicPublisher publisher;
    private TopicSession publishSession;


    public void start() {
        try {
            initReceiver();
            initProducer();
            Scanner scanner = new Scanner(System.in);
            String line = null;
            while (!"-q".equals(line = scanner.nextLine())) {
                TextMessage message = publishSession.createTextMessage();
                message.setText(line);
                publisher.publish(message);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initProducer() throws Exception {
        InitialContext ctx = new InitialContext();
        publishSession = createTopicSession(ctx);
        Topic topic = (Topic) ctx.lookup(TOPIC_NAME);
        publisher = publishSession.createPublisher(topic);
    }

    private void initReceiver() throws Exception {

        InitialContext ctx = new InitialContext();
        TopicSession session =  createTopicSession(ctx);

        Topic topic = (Topic) ctx.lookup(TOPIC_NAME);
        TopicSubscriber receiver = session.createSubscriber(topic);

        receiver.setMessageListener(message -> {
            if  (message instanceof TextMessage) {
                try {
                    System.out.println("received message: " + ((TextMessage) message).getText());

                } catch (JMSException e) {
                    e.printStackTrace();
                }
            }
        });

        System.out.println("Receiver is ready, waiting for messages...");
    }

    private TopicSession createTopicSession(InitialContext ctx) throws Exception {
        TopicConnectionFactory factory = (TopicConnectionFactory) ctx.lookup("topicConnectionFactory");
        TopicConnection connection = factory.createTopicConnection();
        connection.start();

        return connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
    }
}
